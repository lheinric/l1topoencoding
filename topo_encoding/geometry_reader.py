import numpy as np
def load_data(endcap_fwd_file, barrel_file):
    nonbarrel = np.loadtxt(endcap_fwd_file)
    barrel = np.loadtxt(barrel_file)

    eta_ef     = nonbarrel[:,4] + (nonbarrel[:,5]-nonbarrel[:,4])/2
    phi_ef     = nonbarrel[:,6] + (nonbarrel[:,7]-nonbarrel[:,6])/2
    roiid_ef   = nonbarrel[:,3]
    sector_ef  = nonbarrel[:,2]
    dettype_ef = nonbarrel[:,1] 
    side_ef    = nonbarrel[:,0]

    eta_b     = barrel[:,3] + (barrel[:,4]-barrel[:,3])/2
    phi_b     = barrel[:,5] + (barrel[:,6]-barrel[:,5])/2
    roiid_b   = barrel[:,2]
    sector_b  = barrel[:,1]
    side_b    = 1-barrel[:,0]
    dettype_b = 2*np.ones_like(sector_b)


    roiid = np.concatenate([roiid_b,roiid_ef])
    dettype = np.concatenate([dettype_b,dettype_ef])
    side = np.concatenate([side_b,side_ef])
    sector = np.concatenate([sector_b,sector_ef])
    eta = np.concatenate([eta_b,eta_ef])
    phi = np.concatenate([phi_b,phi_ef])



    data = np.stack([
     side,
     dettype,
     sector,
     roiid,
     eta,
     phi
    ],axis=1)
    return data
