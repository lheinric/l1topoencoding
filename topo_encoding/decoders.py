import numpy as np

def decode_barrel(lookup_eta,lookup_phi,encoded, tp= int):
    decoded = []
    for i,e in enumerate(encoded):
        lutcode_eta = e >> 8
        lutcode_phi = e & 255
        eta_index = lutcode_eta >> 1
        lowbits_phi = lutcode_phi & 7
        phi_index = lowbits_phi
        sector = lutcode_phi>>3
        eta = lookup_eta[tp(sector)][eta_index]
        phi = lookup_phi[tp(sector)][phi_index]
                
        decoded.append([eta,phi])
    return np.asarray(decoded)

def decode_endcap(lookup_endcap_eta,lookup_endcap_phi,encoded):
    data = []
    for e in encoded:
        sector = (e >> 8) & 63 # 0-47
        roi_index = e & 255    # 0-147
        
        alternate =  sector % 2
        etacol    = roi_index >> 2
        phirow    = roi_index % 4

        phi = lookup_endcap_phi[sector][phirow]
        eta = lookup_endcap_eta[etacol][alternate]
        data.append([eta,phi,])

    return np.asarray(data)

def decode_forward(lookup_eta,lookup_phi,encoded):
    data = []
    for e in encoded:
        etacode = e >> 8
        phicode = e & 255
        etacol = (etacode >> 1) & 15
        phirow = (phicode >> 1) & 3
        sector = (phicode >> 3)
        alternate = sector % 2
        eta = lookup_eta[alternate][etacol]
        phi = lookup_phi[phirow][sector]
        data.append([eta,phi])
    return np.asarray(data)
