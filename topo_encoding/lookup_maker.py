import json
import numpy as np
from .decoders import *
from .encoders import *

def build_barrel_lookups(data):
    lookup_eta = {}
    lookup_phi = {}
    encode_lookup = {}
    for d in data:
        eta_index, phi_index = None,None
        sector = d[2]
        roiid = d[3]
        eta = d[4]
        phi = d[5]
        lookup_eta.setdefault(int(sector),[])
        lookup_phi.setdefault(int(sector),[])
        try:
            eta_index = lookup_eta[int(sector)].index(eta)
        except ValueError:
            lookup_eta[sector].append(eta)
        eta_index = lookup_eta[int(sector)].index(eta)
        try:
            phi_index = lookup_phi[int(sector)].index(phi)
        except ValueError:
            near = False
            if lookup_phi[int(sector)]:
                distances = [abs(x-phi) for x in lookup_phi[int(sector)]]
                min_exist_dist = np.min(distances)
                argmin = np.argmin(distances)
                if min_exist_dist < 0.0003:
                    near = True
                    pass
                phi_index = argmin
            if not near:
                lookup_phi[sector].append(phi)
                phi_index = lookup_phi[int(sector)].index(phi)
        #this should now always work
        assert eta_index is not None
        assert phi_index is not None
        encode_lookup.setdefault(int(sector),{})[int(roiid)] = [int(eta_index),int(phi_index)]
    return lookup_eta,lookup_phi,encode_lookup

def build_endcap_lookup(data):
    lookup = {}
    for d in data:
        sector = d[2]
        roiid = d[3]
        eta = d[4]
        phi = d[5]
        lookup.setdefault(int(sector),[])
        try:
            roi_index = lookup[sector].index(roiid)
        except ValueError:
            lookup[sector].append([eta,phi])

    lookup = np.asarray([lookup[x] for x in range(48)])
    lookup_endcap_phi = np.asarray([
        [np.mean(lookup[phi,row::4,1])  for row in range(4)]
        for phi in range(48)
    ])

    lookup_endcap_eta= np.asarray([
        [np.mean(lookup[col::2,i:i+4,0]) for col in range(2)] for i in range(0,148,4)]
    )
    return lookup_endcap_eta,lookup_endcap_phi

def build_forward_lookups(data):
    lookup_eta = {}
    lookup_phi = {}
    encoded = []
    for d in data:
        sector = d[2]
        roiid = int(d[3])
        eta = d[4]
        phi = d[5]
        etacol = int(roiid) >> 2
        phirow = int(roiid) & 3
        
        lookup_eta.setdefault(int(sector),{})
        if not etacol in lookup_eta[(sector)]:
            lookup_eta[(sector)][etacol] = eta
        else:
            assert lookup_eta[(sector)][etacol] == eta

        lookup_phi.setdefault(int(sector),{})
        if not phirow in lookup_phi[(sector)]:
            lookup_phi[(sector)][phirow] = phi
        else:
            assert lookup_phi[(sector)][phirow] == phi

    lookups_forward = [lookup_eta, lookup_phi]
    a = np.asarray([[lookups_forward[0][i][j] for i in range(24)] for j in range(16)])
    lookups_forward_phi = np.asarray([[lookups_forward[1][i][j] for i in range(24)] for j in range(4)])
    lookups_forward_eta = np.asarray([np.mean(a[:,::2],axis=1),np.mean(a[:,1::2],axis=1)])
    
    return [lookups_forward_eta,lookups_forward_phi]
        

def process_data_side(data,side):
    data_side0         = data[data[:,0]==side]
    data_side0_barrel  = data_side0[data_side0[:,1]==2]
    data_side0_forward = data_side0[data_side0[:,1]==1]
    data_side0_endcap  = data_side0[data_side0[:,1]==0]


    # build look up tables
    lookups_barrel_eta, lookups_barrel_phi, encode_lookup_barrel = build_barrel_lookups(data_side0_barrel)
    lookup_endcap_eta, lookup_endcap_phi = build_endcap_lookup(data_side0_endcap)
    lookups_forward_eta,lookups_forward_phi = build_forward_lookups(data_side0_forward)


    encoded_barrel = np.asarray(encode_barrel(encode_lookup_barrel,data_side0_barrel))
    decoded_barrel = decode_barrel(
        lookups_barrel_eta,
        lookups_barrel_phi,
        encoded_barrel
    )

    encoded_endcap =encode_endcap(data_side0_endcap)
    decoded_endcap = decode_endcap(
        lookup_endcap_eta,
        lookup_endcap_phi,
        encoded_endcap        
    )

    encoded_forward = encode_forward(data_side0_forward)
    decoded_forward = decode_forward(
        lookups_forward_eta,
        lookups_forward_phi,
        encoded_forward,
    )

    assert np.allclose(decoded_barrel,data_side0_barrel[:,-2:],atol=1e-3)
    assert np.allclose(decoded_endcap,data_side0_endcap[:,-2:],atol=1e-3,rtol=1e-3)
    assert np.allclose(decoded_forward,data_side0_forward[:,-2:])

    encoded = {
        'barrel': encoded_barrel,
        'endcap': encoded_endcap,
        'forward': encoded_forward
    }
    
    lookups =  {
        'encode_lookup_barrel': encode_lookup_barrel,
        'lookup_barrel_eta': lookups_barrel_eta,
        'lookup_barrel_phi': lookups_barrel_phi,
        'lookup_endcap_eta': lookup_endcap_eta,
        'lookup_endcap_phi': lookup_endcap_phi,
        'lookup_forward_eta': lookups_forward_eta,
        'lookup_forward_phi': lookups_forward_phi
    }
    decod_data = {
        'barrel': decoded_barrel,
        'endcap': decoded_endcap,
        'forward': decoded_forward,
    }
    orig_data = {
        'barrel': data_side0_barrel[:,-2:],
        'endcap': data_side0_endcap[:,-2:],
        'forward': data_side0_forward[:,-2:],
    }
    

    return {
        'lookups': lookups,
        'decod': decod_data,
        'orig': orig_data,
        'encod': encoded
    }


def dump_encoding_files(processed,lookup_filename,roi_filename,orig_filename):
    json.dump(
        {
            k:(v.tolist() if type(v)==np.ndarray else v)
            for k,v in processed['lookups'].items()
        },open(lookup_filename,'w')
    )
    json.dump(
        {k:v.tolist() for k,v in processed['encod'].items()
        },
        open(roi_filename,'w')
    )
    json.dump(
        {k:v.tolist() for k,v in processed['orig'].items()
        },
        open(orig_filename,'w')
    )
    
