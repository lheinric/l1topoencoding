import numpy as np

def encode_barrel(encode_lookup,data):        
    encoded = []
    for d in data:
        sector = d[2]
        roiid  = d[3]
        eta_index,phi_index = encode_lookup[sector][roiid]
        if phi_index > 7:
            raise RuntimeError('this should not happen')

        lutcode_eta = eta_index
        lutcode_phi = phi_index & 7
        detector_code = 0
        detector_shift = 5
        etacode = (detector_code << detector_shift) | (lutcode_eta << 1)
        phicode = (int(sector) << 3) | (lutcode_phi)
    
    
        totalcode = (etacode << 8 | phicode)

        encoded.append(totalcode)
    return encoded

def encode_endcap(data):
    encoded = []
    for d in data:
        sector = d[2]
        roiid = d[3]
        detector_code = 1
        detector_shift = 14
        code = (detector_code << detector_shift) | (int(sector) << 8) | int(roiid)
        encoded.append(code)
    return np.asarray(encoded)

def encode_forward(data):
    lookup_phi_forward = {}
    lookup_phi_endcap = {}
    encoded = []
    for d in data:
        sector = d[2]
        roiid = int(d[3])
        etacol = int(roiid) >> 2
        phirow = int(roiid) & 3

        detector_code = 1
        detector_shift = 5
        etacode = (detector_code << detector_shift) | (etacol << 1)
        phicode = (int(sector) << 3) | (phirow << 1)
        
        totalcode = (etacode << 8 | phicode)
        encoded.append(totalcode)
    return np.asarray(encoded  )


