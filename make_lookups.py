#!/usr/bin/env python
import numpy as np

from topo_encoding.geometry_reader import *
from topo_encoding.lookup_maker import *

def make_lookups():
    data = load_data('./Data_RoI_Mapping_EF.txt','./Data_ROI_Mapping_Barrel.txt')
    for side in [0,1]:
        processed = process_data_side(data,side)
        dump_encoding_files(
            processed,
            'lookup_{}.json'.format(side),
            'rois_{}.json'.format(side),
            'orig_{}.json'.format(side)
        )

if __name__ == '__main__':
    make_lookups()
